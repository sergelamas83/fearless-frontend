from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .acls import get_photo, get_weather_data
from .models import Conference, Location, State

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "picture_url",
    ]

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "POST":
        try:
            content = json.loads(request.body)
            print("Received data:", content)
            
            # Get the Location object and put it in the content dict
            location_id = content.get("location")
            if location_id:
                location = Location.objects.get(id=location_id)
                content["location"] = location
            else:
                return JsonResponse(
                    {"message": "Location ID is required"},
                    status=400,
                )

            conference = Conference.objects.create(**content)
            return JsonResponse(
                conference,
                encoder=ConferenceDetailEncoder,
                safe=False,
            )
        except json.JSONDecodeError:
            return JsonResponse(
                {"message": "Invalid JSON data"},
                status=400,
            )
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        except Exception as e:
            return JsonResponse(
                {"error": str(e)},
                status=500,
            )
    elif request.method == "GET":
        try:
            conferences = Conference.objects.all()
            return JsonResponse(
                {"conferences": conferences},
                encoder=ConferenceListEncoder,
            )
        except Exception as e:
            return JsonResponse(
                {"error": str(e)},
                status=500,
            )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, pk):
    try:
        conference = Conference.objects.get(id=pk)
        if request.method == "GET":
            # Get weather data (assuming `get_weather_data` function is defined)
            weather = get_weather_data(
                conference.location.city,
                conference.location.state.abbreviation,
            )
            return JsonResponse(
                {"conference": conference, "weather": weather},
                encoder=ConferenceDetailEncoder,
                safe=False,
            )
        elif request.method == "DELETE":
            count, _ = Conference.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        elif request.method == "PUT":
            content = json.loads(request.body)
            try:
                if "location" in content:
                    location = Location.objects.get(id=content["location"])
                    content["location"] = location
            except Location.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid location id"},
                    status=400,
                )

            Conference.objects.filter(id=pk).update(**content)
            conference = Conference.objects.get(id=pk)
            return JsonResponse(
                conference,
                encoder=ConferenceDetailEncoder,
                safe=False,
            )
    except Conference.DoesNotExist:
        return JsonResponse(
            {"message": "Conference not found"},
            status=404,
        )
    except json.JSONDecodeError:
        return JsonResponse(
            {"message": "Invalid JSON data"},
            status=400,
        )
    except Exception as e:
        return JsonResponse(
            {"error": str(e)},
            status=500,
        )

@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        try:
            locations = Location.objects.all()
            return JsonResponse(
                {"locations": locations},
                encoder=LocationListEncoder,
            )
        except Exception as e:
            return JsonResponse(
                {"error": str(e)},
                status=500,
            )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            state_abbreviation = content.get("state")
            if not state_abbreviation:
                return JsonResponse(
                    {"message": "State abbreviation is required"},
                    status=400,
                )

            try:
                state = State.objects.get(abbreviation=state_abbreviation)
                content["state"] = state
            except State.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid state abbreviation"},
                    status=400,
                )

            # Get photo data (assuming `get_photo` function is defined)
            photo_data = get_photo(content["city"], state_abbreviation)
            content.update(photo_data)

            location = Location.objects.create(**content)
            return JsonResponse(
                location,
                encoder=LocationDetailEncoder,
                safe=False,
            )
        except json.JSONDecodeError:
            return JsonResponse(
                {"message": "Invalid JSON data"},
                status=400,
            )
        except Exception as e:
            return JsonResponse(
                {"error": str(e)},
                status=500,
            )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_location(request, pk):
    try:
        location = Location.objects.get(id=pk)
        if request.method == "GET":
            return JsonResponse(
                location,
                encoder=LocationDetailEncoder,
                safe=False,
            )
        elif request.method == "DELETE":
            count, _ = Location.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        elif request.method == "PUT":
            content = json.loads(request.body)
            state_abbreviation = content.get("state")
            if state_abbreviation:
                try:
                    state = State.objects.get(abbreviation=state_abbreviation)
                    content["state"] = state
                except State.DoesNotExist:
                    return JsonResponse(
                        {"message": "Invalid state abbreviation"},
                        status=400,
                    )

            Location.objects.filter(id=pk).update(**content)
            location = Location.objects.get(id=pk)
            return JsonResponse(
                location,
                encoder=LocationDetailEncoder,
                safe=False,
            )
    except Location.DoesNotExist:
        return JsonResponse(
            {"message": "Location not found"},
            status=404,
        )
    except json.JSONDecodeError:
        return JsonResponse(
            {"message": "Invalid JSON data"},
            status=400,
        )
    except Exception as e:
        return JsonResponse(
            {"error": str(e)},
            status=500,
        )

@require_http_methods(["GET"])
def api_list_states(request):
    try:
        # Get the states from the database ordered by name
        states = State.objects.order_by('name')

        # Create an empty list named state_list
        state_list = []

        # For each state in the states from the database
        for state in states:
            # Create a dictionary that contains the name and abbreviation for each state
            state_data = {
                "name": state.name,
                "abbreviation": state.abbreviation
            }

            # Append the dictionary to the list
            state_list.append(state_data)

        return JsonResponse({"states": state_list})

    except Exception as e:
        # Handle exceptions here
        return JsonResponse({"error": str(e)}, status=500)
