from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import AccountVO, Attendee, ConferenceVO

class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]

class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]

    def get_extra_data(self, o):
        return {"conference": o.conference.name}

class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        count = AccountVO.objects.filter(email=o.email).count()
        return {"has_account": count > 0}

@require_http_methods(["GET"])
def api_list_attendees(request, conference_vo_id=None):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLs. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    if conference_vo_id is not None:
        attendees = Attendee.objects.filter(conference=conference_vo_id)
    else:
        attendees = Attendee.objects.all()

    # Convert attendees to a list of dictionaries
    attendees_data = [
        {
            "name": attendee.name,
            "href": f"/api/attendees/{attendee.id}/",  # Change this URL as needed
        }
        for attendee in attendees
    ]

    return JsonResponse({"attendees": attendees_data})

@require_http_methods(["GET"])
def api_show_attendee(request, pk):
    """
    Returns the details for the Attendee model specified
    by the pk parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    try:
        attendee = Attendee.objects.get(id=pk)
        attendee_data = {
            "email": attendee.email,
            "name": attendee.name,
            "company_name": attendee.company_name,
            "created": attendee.created.strftime("%Y-%m-%d %H:%M:%S"),
            "conference": {
                "name": attendee.conference.name,
                "href": attendee.conference.import_href,
            },
        }
        return JsonResponse(attendee_data, encoder=AttendeeDetailEncoder, safe=False)
    except Attendee.DoesNotExist:
        return JsonResponse(
            {"message": "Attendee not found"},
            status=404,
        )
