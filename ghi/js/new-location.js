console.log('new location page ');

window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/states/';

  try {
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById('state');
      for (let state of data.states) {
        const option = document.createElement('option');
        option.value = state.abbreviation;
        option.innerHTML = state.name;
        selectTag.appendChild(option);
      }

      const formTag = document.getElementById('create-location-form');
      formTag.addEventListener('submit', async (event) => {
        event.preventDefault();

        try {
          const formData = new FormData(formTag);
          const json = JSON.stringify(Object.fromEntries(formData));

          const locationUrl = 'http://localhost:8000/api/locations/';
          const fetchConfig = {
            method: 'POST',
            body: json,
            headers: {
              'Content-Type': 'application/json',
            },
          };

          const response = await fetch(locationUrl, fetchConfig);
          if (response.ok) {
            alert('Location created successfully');
            formTag.reset();
          } else {
            alert('Failed to create location');
          }
        } catch (error) {
          console.error('Error creating location:', error);
          alert('An error occurred while creating the location');
        }
      });
    } else {
      console.error('Failed to fetch states');
    }
  } catch (error) {
    console.error('Error fetching states:', error);
  }
});
