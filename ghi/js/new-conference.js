console.log('new conference page');

document.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/locations/';

  try {
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById('location');
      for (let location of data.locations) {
        const option = document.createElement('option');
        option.value = location.id;
        option.innerHTML = location.name;
        selectTag.appendChild(option);
      }

      const formTag = document.getElementById('create-conference-form');
      formTag.addEventListener('submit', async (event) => {
        event.preventDefault();

        try {
          const formData = new FormData(formTag);
          const json = JSON.stringify(Object.fromEntries(formData));

          const conferenceUrl = 'http://localhost:8000/api/conferences/';
          const fetchConfig = {
            method: 'POST',
            body: json,
            headers: {
              'Content-Type': 'application/json',
            },
          };

          const response = await fetch(conferenceUrl, fetchConfig);
          if (response.ok) {
            alert('Conference created successfully');
            formTag.reset();
          } else {
            alert('Failed to create conference');
          }
        } catch (error) {
          console.error('Error creating conference:', error);
          alert('An error occurred while creating the conference');
        }
      });
    } else {
      console.error('Failed to fetch locations');
    }
  } catch (error) {
    console.error('Error fetching locations:', error);
  }
});
