function createCard(name, description, pictureUrl, startDate, endDate, location) {
    const dateRange = formatDateRange(startDate, endDate);
    return `
      <div class="col-md-4">
        <div class="card" style="width: 18rem;">
          <img src="${pictureUrl}" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <div class="card-footer">
              <small class="location text-muted" style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-size: 13px;">${location}</small>
            </div>
            <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">
            <small class="text-muted"> ${dateRange}</small>
          </div>
        </div>
      </div>
    `;
}

function formatDateRange(startDate, endDate) {
    const options = { year: 'numeric', month: '2-digit', day: '2-digit' };
    const formattedStartDate = new Date(startDate).toLocaleDateString(undefined, options);
    const formattedEndDate = new Date(endDate).toLocaleDateString(undefined, options);
    return `${formattedStartDate} - ${formattedEndDate}`;
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error('Failed to fetch data');
        } else {
            const data = await response.json();
            const conferenceContainer = document.getElementById('conference-container');

            for (const conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const location = details.conference.location.name;
                    const startDate = details.conference.starts;
                    const endDate = details.conference.ends;
                    const html = createCard(title, description, pictureUrl, startDate, endDate, location);

                    conferenceContainer.innerHTML += html;
                }
            }
        }
    } catch (e) {
        console.error(e);
        const errorMessage = document.createElement('div');
        errorMessage.classList.add('alert', 'alert-danger');
        errorMessage.textContent = 'An error occurred while fetching conference data.';
        document.body.prepend(errorMessage);
    }
});
