import React, { useEffect, useState } from 'react';

function LocationForm() {
  const [states, setStates] = useState([]);
  const [name, setName] = useState('');
  const [roomCount, setRoomCount] = useState('');
  const [city, setCity] = useState('');
  const [selectedState, setSelectedState] = useState('');

  const fetchData = async () => {
    try {
      const url = 'http://localhost:8000/api/states/';
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setStates(data.states);
        console.log(data.states);
      } else {
        console.error('HTTP error:', response.status);
      }
    } catch (error) {
      console.error('Fetch error:', error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      room_count: roomCount,
      name: name,
      city: city,
      state: selectedState, // Use selectedState for the state field
    };

    console.log(data);

    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      console.log(newLocation);

      // Clear form fields upon successful submission
      setName('');
      setRoomCount('');
      setCity('');
      setSelectedState('');
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new location</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input
                onChange={(event) => setName(event.target.value)}
                value={name}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(event) => setRoomCount(event.target.value)}
                value={roomCount}
                placeholder="Room count"
                required
                type="number"
                name="room_count"
                id="room_count"
                className="form-control"
              />
              <label htmlFor="room_count">Room count</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(event) => setCity(event.target.value)}
                value={city}
                placeholder="City"
                required
                type="text"
                name="city"
                id="city"
                className="form-control"
              />
              <label htmlFor="city">City</label>
            </div>
            <div className="mb-3">
              <select
                required
                name="state"
                id="state"
                className="form-select"
                onChange={(event) => setSelectedState(event.target.value)}
                value={selectedState}
              >
                <option value="">Choose a state</option>
                {states.map((state) => (
                  <option key={state.abbreviation} value={state.abbreviation}>
                    {state.name}
                  </option>
                ))}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default LocationForm;
