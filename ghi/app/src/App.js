
import React, { useEffect, useState } from 'react';
import Nav from './Nav';
import LocationForm from './LocationForm';

function App() {
  const [attendees, setAttendees] = useState([]);

  useEffect(() => {

  }, []);

  return (
    <>
      <Nav />
      <div className="container">
        <link
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
          crossOrigin="anonymous"
        />
        <LocationForm />

        {/* <AttendeesList attendees={attendees} /> */}
      </div>
    </>
  );
}

export default App;
