//AttendeesList.js
import React from 'react';

function AttendeesList(props) {
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Conference</th>
        </tr>
      </thead>
      <tbody>
        {props.attendees.map((attendee, index) => (
          <tr key={index}>
            <td>{attendee.name}</td>
            <td>{attendee.conference}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default AttendeesList;
